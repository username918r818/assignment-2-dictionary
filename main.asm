%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define SYSCALL_WRITE 1
%define NEWLINE 0xA
%define MAX_WORD_LENGTH 256
%define STD_ERR_FD 2

section .rodata
bad_input_message: db "Bad input", NEWLINE, 0
not_found_message: db "Not found", NEWLINE, 0

section .bss
    input_word: resb MAX_WORD_LENGTH 

section .text
global _start
_start:
    mov rdi, input_word
    mov rsi, MAX_WORD_LENGTH
    call read_word
    test rax, rax
    jz .bad_input
    mov rdi, rax
    mov rsi, word1
    call find_word
    test rax, rax
    jz .not_found
    mov rdi, rax
    call print_string
    call print_newline
    jmp exit

    .bad_input:
        mov rdi, bad_input_message
        jmp .print_error

    .not_found:
        mov rdi, not_found_message
        jmp .print_error

    .print_error:
        push rdi
        call string_length
        mov  rdx, rax 
        mov  rax, SYSCALL_WRITE
        pop  rsi
        mov  rdi, STD_ERR_FD
        syscall
        jmp exit
