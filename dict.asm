%include "lib.inc"

section .text
global find_word

; rdi нуль-терминированная строка
; rsi словарь
find_word: 
    push rsi
    push rdi
    call string_equals
    pop rdi
    pop rsi
    test rax, rax
    jnz .ret_word
    push rdi
    mov rdi, rsi

    call string_length   
    pop rdi 
    inc rax
    mov rsi, [rsi + rax]
    test rsi, rsi
    jnz find_word
    .not_find:
        xor rax, rax
        ret
    .ret_word:
        call string_length
        lea rax, [rax + rsi + 9]
        ret


   

