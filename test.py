import subprocess

input = ["qwe", "wer", "ert", "qw", "qwerty", "testtttttt"]
output = ["wer\n", "ert\n", "rty\n", "", "", ""]
error = ["", "", "", "Not found\n", "Not found\n", "Not found\n"]
flag = True

for i in range(len(input)):
    p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.stdin.write(input[i].encode())
    p.stdin.close()
    st_out_result = p.stdout.read().decode()
    st_err_result = p.stderr.read().decode()

    if st_out_result != output[i]:
        print("Test №", i + 1, "failed. Output: ", st_out_result, "Expected: ", output[i])
        flag = False    
    if st_err_result != error[i]:
        flag = False    
        print("Test №", i + 1, "failed. Error: ", st_err_result, "Expected: ", error[i])

if flag:
    print("All tests passed")