%define SYSCALL_EXIT 60
%define SYSCALL_READ 0
%define SYSCALL_WRITE 1
%define NEWLINE_CHAR 0xA
%define SPACE_CHAR 0x20
%define TAB_CHAR 0x9
%define STD_IN_FD 0
%define STD_OUT_FD 1

global exit 
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, SYSCALL_EXIT   
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor  rax, rax
  .counter:
    cmp  byte [rdi+rax], 0
    je   .end
    inc  rax
    jmp  .counter
  .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov  rdx, rax 
    mov  rax, SYSCALL_WRITE
    pop  rsi
    mov  rdi, STD_OUT_FD
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov  rdx, 1 ; количество символов
    mov  rax, SYSCALL_WRITE
    mov  rsi, rsp
    mov  rdi, STD_OUT_FD
    syscall     
    pop rdi     
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_CHAR
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rdx, rdx
    xor rcx, rcx
    push cx
    add rsp, 1
    mov rax, rdi
    mov rsi, 10
    .loop_div:
        div rsi
        add rdx, '0'; ASCII code of the remainder
        shld rdx, rax, 8
        push dx
        add rsp, 1
        xor rdx, rdx
        inc rcx
        test rax, rax
        jnz .loop_div
    mov rdi, rsp
    push rcx
    call print_string
    pop rax
    add rsp, rax
    inc rsp
    ret 

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    .niggative:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov r8b, byte[rdi]
        mov r9b, byte[rsi]
        inc rdi
        inc rsi
        cmp r8, r9
        jne .not_equal
        test r8b, r8b
        jnz .loop
    mov rax, 1
    ret
    .not_equal:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rdi, STD_IN_FD

    dec rsp
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYSCALL_READ
    syscall
    test al, al
    jz .zero
    mov al, [rsp]
    .zero:
        inc rsp
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:

    push r12
    push r13
    push r14

    mov r12, rdi ; pointer to buffer
    mov r13, rsi ; size
    xor r14, r14 ; counter


    .preloop:
        call .check
        test rax, rax 
        jz .preloop
    .loop:
        cmp r13, r14
        je .bad_ending
        test rax, rax
        je .good_ending
        cmp dl, 0x0
        je .good_ending
        mov [r12 + r14], dl
        inc r14
        call .check
        jmp .loop

    .good_ending:
        mov rax, r12
        mov rdx, r14
        mov byte[r12 + r14], 0x0
        pop r14
        pop r13
        pop r12
        ret

    .bad_ending:
        xor rax, rax
        xor rdx, rdx
        pop r14
        pop r13
        pop r12
        ret

    .check:
        call read_char
        mov dl, al
        cmp al, SPACE_CHAR
        je .whitespace
        cmp al, TAB_CHAR
        je .whitespace
        cmp al, NEWLINE_CHAR
        je .whitespace
        mov rax, 1
        ret
    .whitespace:
        xor rax, rax
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    mov r8, 0xA
    xor rcx, rcx 
    xor rax, rax 
    .loop:
        mov r9b, byte[rdi + rcx]
        sub r9b, '0'
        jb .end
        cmp r9b, 10
        jnb .end
        mul r8
        jc .error
        add rax, r9
        jc .error
        inc rcx
        jmp .loop
    .end:
        mov rdx, rcx
        ret
    .error:
        xor rdx, rdx
        ; mov rax, 1488 ; !!!!!
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .niggative
    cmp byte[rdi], '+'
    je .positive

    jmp parse_uint

    .niggative:
        inc rdi
        call parse_uint
        test rax, rax
        js .error ; проверка на бит знака 
        neg rax 
        test rdx, rdx
        jz .error
        inc rdx
        ret

    .positive:
        inc rdi
        call parse_uint
        test rax, rax
        js .error ; проверка на бит знака 
        test rdx, rdx
        jz .error
        inc rdx
        ret
    
    .error:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    .loop:
        cmp rcx, rdx
        je .bad_ending
        mov r9b, byte[rdi + rcx]
        mov byte[rsi + rcx], r9b
        inc rcx
        cmp r9b, 0x0
        je .good_ending
        jmp .loop

    .bad_ending:
        xor rax, rax
    .good_ending:
        ret
